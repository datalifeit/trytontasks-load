# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model
codes = set()


def load_party(record, party):
    Subdivision = Model.get('country.subdivision')
    Country = Model.get('country.country')
    Category = Model.get('party.category')
    if record['code'] and record['code'] in codes:
        print('Duplicated code - ', record['code'])
    codes.add(record['code'])

    if record.get('street'):
        _address = party.addresses[0]
        for _field in ('street', 'postal_code', 'city'):
            if not record.get(_field, None):
                continue
            setattr(_address, _field, record[_field])
        if record.get('subdivision', None):
            subdivision = Subdivision.find(
                [('name', 'ilike', record['subdivision'])], limit=1)
            if subdivision:
                _address.subdivision = subdivision[0]

        if record.get('country', None):
            country = Country.find(
                [('code', '=', record['country'])], limit=1)
            if country:
                _address.country = country[0]
        if not _address.country and _address.subdivision:
            _address.country = subdivision[0].country

        if record.get('contact', None):
            _address.name = record['contact']
        if record.get('pob'):
            _address.street = (_address.street or '') + ', ' + record['pob']
        if record.get('county', None):
            if record.get('city', None) and record['county'] != record['city']:
                _address.street = (_address.street or '')
                + '. ' + record['county']
            else:
                _address.city = record['county']

    for _field in ('phone', 'mobile', 'website', 'email', 'fax'):
        if not record.get(_field, None):
            continue
        mechanism = party.contact_mechanisms.new()
        mechanism.type = _field
        mechanism.value = record[_field]

    if record.get('vat_number', None):
        vat = party.identifiers.new()
        vat.type = 'eu_vat'
        vat.code = record['vat_number']

    if record.get('category', None):
        cat = Category.find([('name', '=', record['category'])], limit=1)
        if not cat:
            cat = Category(name=record['category'])
        party.categories.append(cat[0])
    return party
