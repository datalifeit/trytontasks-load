# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import ast
import csv
import glob
import os
import re
import pyexcel as pe
from datetime import date
from datetime import timedelta
from decimal import Decimal
from invoke import task
from . import account
from . import party
from . import product
from . import stock
from .tools import (CSV_DELIMITER, timedelta_regex,
    init_logger, get_logger, set_trytond)
from proteus import Model
from trytond.tools import grouped_slice

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser


CUSTOM_METHODS = {
    'account.account': account.load_account,
    'bank.account': account.load_bank_account,
    'account.invoice.payment_term': account.load_payment_term,
    'party.party': party.load_party,
    'product.template': product.load_product_template,
    'account.move.line': account.load_account_move_line,
    'stock.inventory.line': stock.load_inventory_line,
}

DEFAULT_OP = 'c'
DEFAULT_BATCH = 1000
LINE_UP = '\033[1A'
LINE_CLEAR = '\x1b[2K'


@task(help={'config_file': 'Tryton configuration file',
            'database': 'Tryton database name',
            'filename': 'CSV file path to import',
            'modelname': 'Name of model to import. Default value is '
            'file name.',
            'operation': 'Determines the operation to do ('
            'c: always create (default), '
            'w: always write, '
            'cw (or wc): create if not exists otherwise write, '
            'd: delete).',
            'verbose': 'Enable verbose mode',
            'batch': 'Number of iteration records to save'})
def from_csv(ctx, config_file, database, filename,
        modelname='', operation=DEFAULT_OP, verbose=False,
        batch=DEFAULT_BATCH):
    """
    Loads a CSV file into Tryton through Proteus.
    Tricks:
    - creates automatically m2o records if not exists (for simple models
        with non extra required fields).
    - allows to indicate searching field of related model with dot notation
        in column header (ex: party.code)
      Valid for o2m as well.
    - allows to create o2m and m2m records of a given list
        (ex: ["value 1","value 2"])
    - allows to search related model records with many domains.
      Data must be separated by double quotation (ex: "01" "Party 1").
      It is useful for models with rec_name composed by many fields
        (ex: code and name)
    - allows to create a m2o an o2m records with a dict or a list of dicts
        of default values.

    File example:
        code;party;product.code;uoms.symbol;product_category;uom_categories
        01;"01" "Party";P01;["kg","g","lb"];{'name': 'Category 1', 'parent.name': 'Root category'};[{'name': 'Category'}]
    """
    init_logger(verbose)

    set_trytond(config_file, database)

    file_name, file_extension = os.path.splitext(filename)
    assert file_extension == '.csv'

    _load_csv(filename, operation, model_name=modelname, batch=batch)


@task(help={'config_file': 'Tryton configuration file',
            'database': 'Tryton database name',
            'filename': 'CFG file path to import',
            'verbose': 'Enable verbose mode'})
def from_cfg(ctx, config_file, database, filename, verbose=False):
    """
    Loads a CFG file (with CSV file sections) into Tryton through Proteus.
    Tricks:
    - creates automatically m2o records if not exists (for simple models
        with non extra required fields).
    - allows to indicate searching field of related model with dot notation
        in column header (ex: party.code)
      Valid for o2m as well.
    - allows to create o2m and m2m records of a given list
        (ex: ["value 1","value 2"])
    - allows to search related model records with many domains.
      Data must be separated by double quotation (ex: "01" "Party 1").
      It is useful for models with rec_name composed by many fields
        (ex: code and name)
    - allows to create a m2o an o2m records with a dict or a list of dicts
        of default values.

    File example:
        code;party;product.code;uoms.symbol;product_category;uom_categories
        01;"01" "Party";P01;["kg","g","lb"];{'name': 'Category 1'};[{'name': 'Category'}]

    CFG File options:
    [section]
    file = <relative path to file>
    model = <model name if distinct of file name>
    op = <operation type if distinct of create>
    """
    init_logger(verbose)

    set_trytond(config_file, database)

    file_name, file_extension = os.path.splitext(filename)
    assert file_extension in ('.cfg', '.conf')
    cp = ConfigParser()
    cp.read(filename)

    for section in cp.sections():
        print('Loading %s' % cp.get(section, 'file'))
        _path = cp.get(section, 'file')
        if cp.get(section, 'file').startswith('./'):
            _path = os.path.split(file_name)[0] + cp.get(section, 'file')[1:]
        res = _load_csv(_path,
            cp.get(section, 'op') if cp.has_option(section, 'op')
                else DEFAULT_OP,
            model_name=cp.get(section, 'model') if cp.has_option(section,
                'model') else None,)
        if not res:
            break


@task(help={'config_file': 'Tryton configuration file',
            'database': 'Tryton database name',
            'filename': 'Excel file path to import',
            'metadatafile': 'Configuration file of excel sheets. '
            'If missing will be used <config_file>.conf',
            'verbose': 'Enable verbose mode'})
def from_excel(ctx, config_file, database, filename, metadatafile=None,
        verbose=False):
    """
    Loads an XLS/ODS file with many sheets into Tryton through Proteus.
    A metadata configuration file is required.

    Configuration file options:
    [section]
    sheet = <sheet name>
    model = <model name>
    headers:
        code
        name
        <other column headers>
    converter_<header> = {"key1": "value1", ...} # Converts user friendly
        values into field accepted values
    prefix_<header> = <text to prefix> # Adds a fixed prefix to value
    """

    init_logger(verbose)

    set_trytond(config_file, database)

    file_name, file_extension = os.path.splitext(filename)
    assert file_extension in ('.xls', '.xlsx', '.ods')

    # convert excel into csv
    dest_file_name = os.path.split(file_name)[0] + '/tmp/'
    if not os.path.exists(dest_file_name):
        os.mkdir(dest_file_name)
    else:
        filelist = glob.glob(dest_file_name + '*.csv')
        for f in filelist:
            os.remove(f)

    pe.save_book_as(
        file_name=filename, dest_file_name=dest_file_name + 'data.csv',
        dest_delimiter=CSV_DELIMITER, dest_quoting=csv.QUOTE_NONNUMERIC)

    # read metadata file
    cp = ConfigParser()
    cp.read(metadatafile or '%s.conf' % file_name)

    if not cp.sections():
        raise Exception('Non valid metadata file "%s"' % (
            metadatafile or '%s.conf' % file_name))
    for section in cp.sections():
        _Model = Model.get(cp.get(section, 'model'))
        csv_filename = 'data__%s__' % cp.get(section, 'sheet')
        csv_file = glob.glob(dest_file_name + '%s*.csv' % csv_filename)
        assert len(csv_file) == 1
        csv_file, = csv_file

        # format headers
        outputFileName = os.path.splitext(csv_file)[0] + '_modified.csv'

        converters = {}
        # default converters
        for key, conv in cp.defaults().items():
            converters[key.replace('converter_', '')] = ast.literal_eval(conv)

        prefixes = {}
        with open(csv_file, 'rb') as inFile, \
                open(outputFileName, 'wb') as outfile:
            r = csv.reader(inFile, delimiter=CSV_DELIMITER)
            w = csv.writer(outfile, delimiter=CSV_DELIMITER)

            old_headers = r.next()
            headers = cp.get(section, 'headers').strip().splitlines()
            assert len(old_headers) == len(headers)
            # write new headers
            w.writerow(headers)

            # apply converters, prefixes
            for head in headers:
                _head = head.split('.')[0]
                if cp.has_option(section, 'converter_%s' % _head):
                    converters.update({head: ast.literal_eval(
                        cp.get(section, 'converter_%s' % _head))})
                if cp.has_option(section, 'prefix_%s' % _head):
                    prefixes.update({
                        head: cp.get(section, 'prefix_%s' % _head)})

            for row in r:
                _row = row + ([''] * (len(headers) - len(row)))
                cols = []
                for ii, head in enumerate(headers):
                    value = _row[ii]
                    field_type_ = (_Model._fields[head]['type']
                                   if _Model._fields.get(head, None) else None)
                    if field_type_ and field_type_ in converters:
                        value = converters[field_type_][_row[ii]]
                    if head in converters:
                        value = converters[head][_row[ii]]
                    if head in prefixes and value:
                        value = prefixes[head] + value
                    cols.append(value)
                w.writerow(cols)
        os.remove(csv_file)
        os.rename(outputFileName, csv_file)

        # load data
        _load_csv(csv_file,
            cp.get(section, 'op') if cp.has_option(section, 'op')
                else DEFAULT_OP,
            model_name=cp.get(section, 'model') if cp.has_option(section,
                'model') else None)


def _load_csv(csv_filename, operation, model_name=None, batch=DEFAULT_BATCH):

    to_read = open(csv_filename, 'rt')
    csv_file = csv.reader(
        to_read, delimiter=CSV_DELIMITER)
    _file_name, _file_extension = os.path.splitext(csv_filename)

    if not model_name:
        model_name = _file_name[_file_name.rfind('/') + 1:]
    try:
        TrytonModel = Model.get(model_name)
    except Exception as e:
        get_logger().error('Model error for %s ' % model_name)
        get_logger().error(str(e))
        return False

    def search(search_model, search_column, search_value):
        """Allows to find a record model from a column value"""
        _domain = []
        if search_value.endswith('"'):
            # Correct problem with double quotes remove in file reading
            _index = search_value.index(' "')
            if search_value[_index - 1] == '"':
                _index -= 1
            search_value = '"%s" %s' % (
                search_value[:_index],
                search_value[search_value.index(' "') + 1:])
            values = re.findall(r'[^"\s]\S*|".+?"', search_value)
            for v in values:
                _domain.append((search_column, '=', str(v).strip('"')))
        else:
            _domain.append((search_column, '=', search_value))

        get_logger().info('Searching in model "%s" by the following '
            'criteria:' % search_model)
        get_logger().info(_domain)

        records = Model.get(search_model).find(_domain)
        if records:
            return records[0]
        get_logger().warning(' Record "%s" does not exists on model "%s"! '
            'Process will try to create it.' % (search_value, search_model))
        return None

    def read_column(col_header):
        """Reads a column definition of csv file"""
        _index = headers.index(col_header)
        column_value = col_header
        if '.' in column_value:
            _col_header, _dest_name = str(column_value).split('.', 1)
        else:
            _col_header = column_value
            _dest_name = 'rec_name'
        return _index, _col_header, _dest_name

    def get_column_value(value):
        try:
            if value.startswith('{') and value.endswith('}'):
                return ast.literal_eval(value)
            if value.startswith('[') and value.endswith(']'):
                return ast.literal_eval(value)
        except ValueError as e:
            get_logger().error('%s, %s' % (str(e), value))
            raise e

        return value

    def _parse_complex_one2many(model_name, value):
        if isinstance(value, dict):
            for field, field_value in value.items():
                if '.' not in field:
                    continue
                One2manyModel = Model.get(model_name)
                m2o, field_m2o = field.split('.')
                _type = One2manyModel._fields[m2o]['type']
                if _type == 'many2one':
                    new_value = search(One2manyModel._fields[m2o]['relation'],
                        field_m2o, field_value)
                    value.pop(field, None)
                    value[m2o] = new_value.id if new_value else None

    records = []
    headers = next(csv_file)
    csv_file = list(csv_file)
    num_records = len(list(csv_file))
    record2row = {}
    for index, row in enumerate(csv_file):
        record = None
        if 'w' in operation:
            _, col_name, _ = read_column(headers[0])
            get_logger().warning('Search record "%s"' % row[0])
            record = TrytonModel.find([(col_name, '=', row[0])])
            if record:
                record, = record
            else:
                record = None
                get_logger().warning(' Record "%s" does not exists on model '
                    '"%s"! Process will try to create it.' % (
                        row[0], model_name))
        if record is None and 'c' in operation:
            record = TrytonModel()
        if CUSTOM_METHODS.get(model_name, None):
            try:
                row_dict = {h: row[ii] for ii, h in enumerate(headers)}
            except Exception as e:
                get_logger().error("Check number of headers "
                    "and values are the same. Error:{}".format(e.message))
                return False
            record = CUSTOM_METHODS[model_name](row_dict, record)
            if not record:
                continue

        for column in headers:
            col_index, column_name, dest_name = read_column(column)
            if not record._fields.get(column_name):
                get_logger().info('Column "%s" in model "%s" ignored!!' %
                                  (column_name, model_name))
                continue
            type_ = record._fields[column_name]['type']
            if not row[col_index]:
                continue
            elif type_ == 'integer':
                setattr(record, column_name, int(row[col_index]))
            elif type_ == 'float':
                setattr(record, column_name, float(
                    row[col_index].replace(',', '.')))
            elif type_ == 'numeric':
                setattr(record, column_name, Decimal(row[col_index]))
            elif type_ == 'boolean':
                value = str(row[col_index]).lower() in (
                    'true', '1', 'on', 'yes', 'si')
                setattr(record, column_name, value)
            elif type_ == 'timedelta':
                def parse_timedelta(time_str):
                    parts = timedelta_regex.match(time_str)
                    if not parts:
                        return
                    parts = parts.groupdict()
                    time_params = {}
                    for (name, param) in parts.items():
                        if param:
                            time_params[name] = int(param)
                    return timedelta(**time_params)

                value = parse_timedelta(row[col_index])
                setattr(record, column, value)
            elif type_ == 'date':
                (year, month, day) = [
                    int(d) for d in row[col_index].split('-')]
                datevalue = date(year, month, day)
                repr(datevalue)
                setattr(record, column_name, datevalue)
            elif type_ in ('one2many', 'many2many'):
                model = record._fields[column_name]['relation']
                list_value = get_column_value(row[col_index]) or []
                if not isinstance(list_value, list):
                    raise TypeError('"%s" must be a list!!' % list_value)
                if operation == 'c':
                    # default values removed as we are creating new record
                    while getattr(record, column_name):
                        getattr(record, column_name).pop()
                for item in list_value:
                    result = None
                    value = item
                    if not isinstance(item, dict):
                        result = search(model, dest_name, item)
                        value = {dest_name: item}
                    if result:
                        func = getattr(getattr(record, column_name), 'append')
                        func(result)
                    elif type_ == 'one2many':
                        _parse_complex_one2many(model, value)
                        func = getattr(getattr(record, column_name), 'new')
                        _ = func(**value)
                    elif type_ == 'many2many':
                        _parse_complex_one2many(model, value)
                        func = getattr(getattr(record, column_name), 'new')
                        _ = func(**value)
            elif type_ in ('many2one', 'one2one'):
                model = record._fields[column_name]['relation']
                value = get_column_value(row[col_index])
                result = None
                if not isinstance(value, dict):
                    result = search(model, dest_name, value)
                    value = {dest_name[4:] if dest_name == 'rec_name'
                        else dest_name: value}
                if dest_name == 'rec_name':
                    dest_name = dest_name[4:]
                if result:
                    setattr(record, column_name, result)
                else:
                    RelatedModel = Model.get(
                        record._fields[column_name]['relation'])
                    related_record = RelatedModel(**value)
                    related_record.save()
                    setattr(record, column_name, related_record)
            elif type_ == 'multiselection':
                setattr(record, column_name, ast.literal_eval(row[col_index]))
            else:
                setattr(record, column_name, row[col_index])

        records.append(record)
        record2row[record] = row
        print('>>> Processed %d records of %d' % (index + 1, num_records))
        print(LINE_UP, end=index + 1 != num_records and LINE_CLEAR or None)

    # Close file
    to_read.close()

    try:
        last_saved = None
        last_to_save = None
        saved_records = 0
        for to_save_group in grouped_slice(records, batch):
            to_save_group = list(to_save_group)
            last_to_save = CSV_DELIMITER.join(record2row[to_save_group[-1]])
            TrytonModel.save(to_save_group)
            saved_records += len(to_save_group)
            print('>>> Saved %d records of %d' % (saved_records, num_records))
            if saved_records / num_records != 1:
                print(LINE_UP, end=LINE_CLEAR)
            last_saved = last_to_save

    except Exception as e:
        if last_saved:
            print('>>> Last row saved correctly:\n\t%s' % last_saved)
        get_logger().error(e.message)
        return False
    return True
