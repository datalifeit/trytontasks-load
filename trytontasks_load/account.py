# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model
from .tools import get_logger, get_tryton_context, _CONFIG


def load_account(record, account):
    Account = Model.get('account.account')
    Configuration = Model.get('account.configuration')

    code_length = getattr(Configuration(1), 'default_account_code_digits', 8)

    account = Account.find(
        [('code', '=', record['code']),
         ('company', '=', record.get('company', 1))], limit=1)
    if account:
        return account[0]

    min_length = min([code_length, 4])
    template_code = ('{:0<' + str(code_length) + '}').format(
        record['code'][:min_length])
    template_account = Account.find([
        ('code', '=', template_code),
        ('company.id', '=', record.get('company', 1))], limit=1)
    if not template_account:
        min_length = min([code_length, 3])
        template_code = ('{:0<' + str(code_length) + '}').format(
            record['code'][:min_length])
        template_account = Account.find([
            ('code', '=', template_code),
            ('company.id', '=', record.get('company', 1))], limit=1)

    if not template_account:
        get_logger().error(
            'Cannot find an account template in order to create account "%s". '
            'Record will not be created!!' % record['code'])
        return None
    account, = Account.duplicate(template_account, default={
        'code': record['code'], 'name': record['name']
    })


def load_bank_account(record, bank):
    Party = Model.get('party.party')

    for f_type, f in (('iban', 'iban'), ('other', 'other_number')):
        if f not in record or not record[f]:
            continue
        _number = bank.numbers.new()
        _number.type = f_type
        _number.number = record[f]

    result = [(key, value) for key, value in record.items()
        if key.startswith('party')]
    if result and result[0][1]:
        result, = result
        _, value = result[0].split('.')
        party = Party.find([(value, '=', result[1])], limit=1)
        if party:
            bank.owners.append(party[0])

    return bank


def load_payment_term(record):
    Term = Model.get('account.invoice.payment_term')

    term = Term()
    if record.get('days'):
        line = term.lines.new()
        _delta = line.relativedeltas.new()
        _delta.days = int(record['days'])

    return term


def load_account_move_line(record, move_line):
    """Considerations:
        - account move must exists
        - add date column in csv to not modify move date
    """

    Move = Model.get('account.move')

    for key, value in record.items():
        if key.startswith('move'):
            key_value = key
            break
    new_key = key_value.split('.')
    new_key = 'rec_name' if len(new_key) == 1 else new_key[1]
    move, = Move.find([(new_key, '=', record[key_value])])
    get_tryton_context().update({
        'journal': move.journal.id,
        'period': move.period.id,
        'date': move.date
    })
    return move_line
