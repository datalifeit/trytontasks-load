# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from proteus import Model


def load_inventory_line(record, line):
    Product = Model.get('product.product')
    Lot = None

    try:
        Lot = Model.get('stock.lot')
    except KeyError:
        pass

    _product_col = 'product'
    for key in record.iterkeys():
        if key.startswith('product'):
            _product_col = key
            break

    if record.get('lot_number'):
        lot = Lot.find([
            ('number', '=', record['lot_number']),
            (_product_col, '=', record[_product_col])])
        if lot:
            if len(lot) > 1:
                print('Lot %s duplicated' % record['lot_number'])
            lot = lot[0]
        else:
            _col = _product_col[7:]
            if _col.startswith('.'):
                _col = _col[1:]
            product = Product.find([
                (_col, '=', record[_product_col])])
            if len(product) > 1:
                print('Product duplicated %s' % product[0].code)
            if not product:
                raise Exception('Product %s not found!' % record[_product_col])
            product = product[0]
            lot = Lot(product=product, number=record['lot_number'])
            lot.save()
        line.lot = lot
    return line
